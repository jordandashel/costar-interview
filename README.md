# Co--Star Friends Platform

## How to Run

The easiest way to run this program is with docker. There is an image,
hosted on Docker hub.

```
$ docker run -d -p 5000:5000 docker.io/jordandashel/costar_interview:latest
```

If you want to run the program locally, you will need `python3`, `pip`, etc. I would
recommend using a virtual environment. The dependencies can be installed with 

```
$ pip install requirements.txt
```

You need to have the variable `FLASK_APP` set in your env so that flask can find
it

```
$ export FLASK_APP=main.py
```

The database can be initialized with 

```
$ python -m flask initdb
```

It can also be created using `sqlite3` or simply `flask initdb`.
You can then run the program.

```
$ python main.py
```

The program should then be running on localhost at port 5000. You can also run
with `flask run`.

## How to Use

To interact with the program, you can follow the spec:

To create a new user:

```
POST http://localhost:5000/user {"Username": "wendy", "Birthdate": "01/26/64"}
```
Note... Dates must be presented as "dd/mm/yy". Making this more flexible is a
piece I'd work on with more time.

To see all users:

```
GET http://localhost:5000/user
```

If you would like to establish a new friend, use this endpoint:

```
POST http://localhost:5000/new-friend {"Username": "wendy", "Friend": "lisa"}
```

And you can see a user's friends with

```
GET http://localhost:5000/friends/lisa
```

Note: according to the spec, only friends who are mutual are shown. Lisa must be
friends with wendy and wendy must be friends with lisa to appear on each other's
friend list.


## Todo Items

Given more time to spend working on this program, there are a number of things
I'd like to take care of.

 - Improved test coverage

 Test coverage is minimal. I'd especially like to get some tests around the
 database operations, mocking out the db to test the calls made to it. There
 also could be more functional tests of the API.

 - Data verification

 The program takes the user at their word. There is no verification
 of anything going into the database. It would be nice to validate and
 check what is being saved to a database, normalizing dates, perhaps with a ui
 date chooser, as well as definitely more sophisticated date parsing.

 - User uniqueness

A user can register as many times as they like. There is no restriction on user
creation (an extension of the fact that no user input is really checked). I'd
like to make a username unique.

 - Fail well

 The program has little tolerance for failure, for example, requesting the
 friends of a user that does not exist.

etc.
