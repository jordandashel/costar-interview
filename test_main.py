import os
import unittest
import tempfile

import main

class MainTestCase(unittest.TestCase):

    def setUp(self):
        self.app = main.app.test_client()

    def tearDown(self):
        pass


    def test_get_response(self):
        response = self.app.get('/')
        assert response.status_code is 200

if __name__ == '__main__':
    unittest.main()
