from datetime import datetime

def get_dt(month, day):
    return datetime.strptime("%s/%s" % (month, day), "%m/%d")

# Capricorn is split into two dates to prevent a comparison 
# failure at the new year rollover (01 > 12)
signs = [
    {"sign": "Aries", 
        "start_date":   get_dt("03", "21"),
        "end_date":     get_dt("04", "19")},
    {"sign": "Taurus", 
        "start_date":   get_dt("04", "20"),
        "end_date":     get_dt("05", "20")},
    {"sign": "Gemini", 
        "start_date":   get_dt("05", "21"),
        "end_date":     get_dt("06", "20")},
    {"sign": "Cancer", 
        "start_date":   get_dt("06", "21"),
        "end_date":     get_dt("07", "22")},
    {"sign": "Leo", 
        "start_date":   get_dt("07", "23"),
        "end_date":     get_dt("08", "22")},
    {"sign": "Virgo", 
        "start_date":   get_dt("08", "23"),
        "end_date":     get_dt("09", "22")},
    {"sign": "Libra", 
        "start_date":   get_dt("09", "23"),
        "end_date":     get_dt("10", "22")},
    {"sign": "Scorpio", 
        "start_date":   get_dt("10", "23"),
        "end_date":     get_dt("11", "21")},
    {"sign": "Sagittarius", 
        "start_date":   get_dt("11", "22"),
        "end_date":     get_dt("12", "21")},
    {"sign": "Capricorn", 
        "start_date":   get_dt("12", "22"),
        "end_date":     get_dt("12", "31")},
    {"sign": "Capricorn", 
        "start_date":   get_dt("01", "01"),
        "end_date":     get_dt("01", "19")},
    {"sign": "Aquarius", 
        "start_date":   get_dt("01", "20"),
        "end_date":     get_dt("02", "18")},
    {"sign": "Pisces", 
        "start_date":   get_dt("02", "19"),
        "end_date":     get_dt("03", "20")},

    ]



# Given how dates are represented in the DB, we
# can expect the date to be a String: "mm/dd/yy"
# TODO: unbind date representations from string definition
def convert_date_to_sign(date):
    month, day, year = date.split('/')
    date = get_dt(month, day)
    for sign in signs:
        if date >= sign["start_date"] and date <= sign["end_date"]:
            return sign["sign"]
