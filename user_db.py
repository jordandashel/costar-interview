import os
from sqlite3 import dbapi2 as sqlite3 
from flask import g
import main as main


# TODO: No real sanitization or validation going on here.
def add_user(username, birthdate):
    user_db = _get_db()
    user_db.execute(
            'INSERT INTO users (username, birthdate) values (?, ?)',
            (username, birthdate))
    user_db.commit()

def get_all_users():
    user_db = _get_db()
    cur = user_db.execute('select username, birthdate from users')
    entries = cur.fetchall()
    return entries

def get_birthdate(username):
    user_db = _get_db()
    cur = user_db.execute('select birthdate from users where username=?', (username,))
    entry = cur.fetchone()
    return entry[0]

def get_friends(username):
    user_id = _get_user_key(username)[0]
    associated_friends = _select_friends(user_id)
    user_friends = []
    for friend in associated_friends:
        user_friends += _get_user_name(friend[0])
    return user_friends

def make_friendship(username, new_friend):
    user_key = _get_user_key(username)
    friend_key = _get_user_key(new_friend)
    _add_friend(user_key, friend_key)

def is_friend(user1, user2):
    user_1_friends = get_friends(user1)
    user_2_friends = get_friends(user2)
    return user2 in user_1_friends and user1 in user_2_friends

def _get_user_name(user_key):
    user_db = _get_db()
    cur = user_db.execute('select username from users where key=?', 
            (user_key,))
    entry = cur.fetchone()
    return entry

def _get_user_key(username):
    user_db = _get_db()
    cur = user_db.execute('select key from users where username=?', (username,))
    entry = cur.fetchone()
    return entry

def _add_friend(user_key, new_friend_key):
    friend_db = _get_db()
    friend_db.execute(
            'INSERT INTO friends (user_id, friend_id) values (?, ?)',
            (int(user_key[0]), int(new_friend_key[0])))
    friend_db.commit()

def _select_friends(user_key):
    user_db = _get_db()
    cur = user_db.execute('select friend_id from friends where user_id=?', 
            (user_key,))
    entries = cur.fetchall()
    return entries

def _connect_db():
    rv = sqlite3.connect(main.app.config['USER_DATABASE'])
    rv.row_factory = sqlite3.Row
    return rv

def _get_db():
    if not hasattr(g, 'sqlite_db'):
        g.sqlite_db = _connect_db()
    return g.sqlite_db

def init_db():
    db = _get_db()
    with main.app.open_resource('schema.sql', mode='r') as f:
        db.cursor().executescript(f.read())
    db.commit()
