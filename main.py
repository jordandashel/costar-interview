from flask import Flask, request, render_template, Response

import json
import os

import user_db
import sun_signs

app = Flask(__name__)

app.config.from_object(__name__)

# Dummy db data for proof of concept. In reality, secret key, 
# username, and password would be actual secret keys, usernames,
# and passwords, respectively.
app.config.update(
    USER_DATABASE=os.path.join(app.root_path, 'users.db'),
    SECRET_KEY=b'4\x81\xc4\x8a\x0f\xcc\xaa\x17Q\xde\x93\x9d',
    USERNAME='admin',
    PASSWORD='default'
)

# TODO: support db failure (duplicate user, invalid data etc.)
@app.route('/user', methods=["GET", "POST"])
def add_user():
    if request.method == "POST":
        new_user_data = request.get_json()
        user_db.add_user(new_user_data['Username'], new_user_data['Birthdate'])
        success_message = {"message": "thank you %s; success!" % new_user_data["Username"] }
        return Response(json.dumps(success_message), status=201, mimetype='application/json')
    else:
        all_users = user_db.get_all_users()
        return render_template('all_users.html', users=all_users)

@app.route('/new-friend', methods=["POST"])
def make_friends():
    new_friendship_data = request.get_json()
    username = new_friendship_data['Username']
    new_friend = new_friendship_data['Friend']
    user_db.make_friendship(username, new_friend)
    return "Friendship made :)", 201

# TODO: fails poorly when looking up a nonexistant user
@app.route('/friends/<username>')
def friends(username):
    all_friends = user_db.get_friends(username)

    # only returning mutual friends
    true_friends = []
    for friend in all_friends:
        if user_db.is_friend(friend, username):
            true_friends.append({"Username": friend, 
                                "Birthdate": user_db.get_birthdate(friend)})
    return render_template('all_users.html', users=true_friends)

@app.context_processor
def utility_processor():
    def convert_to_sign(date):
        return sun_signs.convert_date_to_sign(date)
    return dict(convert_to_sign=convert_to_sign)

@app.route('/')
def home():
    return "Welcome"

@app.cli.command('initdb')
def initdb_command():
    user_db.init_db()
    print('Initialized the database.')


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
