import os
import unittest
import tempfile

import sun_signs

class SunSignTestCase(unittest.TestCase):

    def test_convert_date_to_sign(self):
        date = "03/29/88"
        sign = sun_signs.convert_date_to_sign(date)
        self.assertEqual(sign, "Aries")

        date = "02/28/88"
        sign = sun_signs.convert_date_to_sign(date)
        self.assertEqual(sign, "Pisces")

        date = "11/28/88"
        sign = sun_signs.convert_date_to_sign(date)
        self.assertEqual(sign, "Sagittarius")

        date = "01/03/88"
        sign = sun_signs.convert_date_to_sign(date)
        self.assertEqual(sign, "Capricorn")

    def test_get_dt(self):
        month = "01"
        day = "05"
        date = sun_signs.get_dt(month, day)
        self.assertEqual(date.month, 1)
        self.assertEqual(date.day, 5)


if __name__ == '__main__':
    unittest.main()
