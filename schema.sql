DROP TABLE IF EXISTS users;
CREATE TABLE users (
  key integer PRIMARY KEY AUTOINCREMENT NOT NULL,
  username text NOT NULL,
  birthdate text NOT NULL
);
DROP TABLE IF EXISTS friends;
CREATE TABLE friends (
  key integer PRIMARY KEY AUTOINCREMENT NOT NULL,
  user_id integer NOT NULL,
  friend_id integer NOT NULL
)
